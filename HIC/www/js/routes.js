angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('start', {
    url: '/page5',
    templateUrl: 'templates/start.html',
    controller: 'startCtrl'
  })

  .state('login', {
    url: '/page6',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('signup', {
    url: '/page7',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('tabsController.eListe', {
    url: '/page8',
    views: {
      'tab1': {
        templateUrl: 'templates/eListe.html',
        controller: 'eListeCtrl'
      }
    }
  })

  .state('tabsController.inventory', {
    url: '/page9',
    views: {
      'tab2': {
        templateUrl: 'templates/inventory.html',
        controller: 'inventoryCtrl'
      }
    }
  })

  .state('tabsController.kamera', {
    url: '/page10',
    views: {
      'tab4': {
        templateUrl: 'templates/kamera.html',
        controller: 'kameraCtrl'
      }
    }
  })

  .state('tabsController.scanErgebnis', {
    url: '/page11',
    views: {
      'tab4': {
        templateUrl: 'templates/scanErgebnis.html',
        controller: 'scanErgebnisCtrl'
      }
    }
  })

  .state('tabsController.profile', {
    url: '/page12',
    views: {
      'tab3': {
        templateUrl: 'templates/profile.html',
        controller: 'profileCtrl'
      }
    }
  })

  .state('tabsController.keinCodeInDBVorhanden', {
    url: '/page13',
    views: {
      'tab4': {
        templateUrl: 'templates/keinCodeInDBVorhanden.html',
        controller: 'keinCodeInDBVorhandenCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/page5')

  

});