angular.module('app.controllers', [])

.controller('startCtrl', function($scope) {

})

.controller('loginCtrl', function($scope) {

})

.controller('signupCtrl', function($scope) {

})

.controller('eListeCtrl', function($scope,$http) {

  $http({
    method: 'get',
    url: 'http://testhic-foi15a1.rhcloud.com/api/getProductShplist'
  }).then(function successCallback(response) {
    $scope.productsShplist = response.data
    console.log(response);
  }, function errorCallback(response) {
    console.log(response);
  });
})

.controller('inventoryCtrl', function($scope,$http,$window) {
  $http({
    method: 'get',
    url: 'http://testhic-foi15a1.rhcloud.com/api/getProductInv'
  }).then(function successCallback(response) {
    $scope.productsInv = response.data
    console.log(response);
  }, function errorCallback(response) {
    console.log(response);
  });



  $scope.updateInventoryGet = function(){
    $http({
      method: 'get',
      url: 'http://testhic-foi15a1.rhcloud.com/api/getProductInv'
    }).then(function successCallback(response) {
      $scope.productsInv = response.data
      console.log(response);
    }, function errorCallback(response) {
      console.log(response);
    });
  }

  $scope.countFunction = function(id,operator){
    var el = angular.element( document.querySelector( '#proanz_'+id ) );
    var value = parseInt(el.text(),10);
    if(operator == "+")
    value = value + 1
    else {
      var val = value - 1
      if((value - 1) >= 0)
      value = value - 1
    }
    el.html(value)

    $scope.updateProduktAnzahl(value,id)
    return value
  }

  $scope.updateProduktAnzahl = function(anz,id){
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/setProAnzInv',
      data: {ProAnz:anz,productid:id}
    }).then(function successCallback(response){
      console.log(response);
      $scope.updateInventoryGet
    }, function errorCallback(response){
      console.log(response.data);
    });
  }

  $scope.deleteProductFromInventory = function(product_id,inventory_id) {
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/deleteProductInv',
      data: {'pro_id':product_id,'inv_id':inventory_id}
    }).then(function successCallback(response){
      $window.location.reload();
      console.log(response);
    }, function errorCallback(response){
      console.log(response.data);
    });
  }

  $scope.transferToShoppingList = function(product_id,inventory_id) {
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/transferToShoppingList',
      data: {'pro_id':product_id,'inv_id':inventory_id}
    }).then(function successCallback(response){
      console.log(response);
    }, function errorCallback(response){
      console.log(response.data);
    });
  }

})

.controller('kameraCtrl', function($scope) {

})

.controller('scanErgebnisCtrl', function($scope) {

})

.controller('profileCtrl', function($scope) {

})

.controller('keinCodeInDBVorhandenCtrl', function($scope) {

})
