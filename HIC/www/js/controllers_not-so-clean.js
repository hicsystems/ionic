angular.module('app.controllers', [])

.controller('startCtrl', function($scope) {

})

.controller('loginCtrl', function($scope) {

})

.controller('signupCtrl', function($scope) {

})

.controller('eListeCtrl', function($scope,$http) {

  $http({
    method: 'get',
    url: 'http://testhic-foi15a1.rhcloud.com/api/getProductShplist'
  }).then(function successCallback(response) {
    $scope.productsShplist = response.data
    console.log(response);
  }, function errorCallback(response) {
    console.log(response);
  });
})

.controller('inventoryCtrl', function($scope,$http,$window) {
  $http({
    method: 'get',
    url: 'http://testhic-foi15a1.rhcloud.com/api/getProductInv'
  }).then(function successCallback(response) {
    $scope.productsInv = response.data
    console.log(response);
  }, function errorCallback(response) {
    console.log(response);
  });

  $scope.updateInventoryGet = function(){
    $http({
      method: 'get',
      url: 'http://testhic-foi15a1.rhcloud.com/api/getProductInv'
    }).then(function successCallback(response) {
      $scope.productsInv = response.data
      console.log(response);
    }, function errorCallback(response) {
      console.log(response);
    });
  }

  /*$scope.updateProduktAnzahlPositivGet = function(anz,id){
    anz = anz+1
    el = "ProAnz_"+id;
    $scope.el = anz;
    $scope.updateProduktAnzahlPositivPost(anz)
  }*/

  $scope.countFunction = function(id,operator){
    var el = angular.element( document.querySelector( '#proanz_'+id ) );
    var value = parseInt(el.text(),10);
    if(operator == "+")
      value = value + 1
    else {
      var val = value - 1
      if((value - 1) >= 0)
        value = value - 1
    }
    el.html(value)

    updateProduktAnzahl(value,id)//funkction zum posten in die DB hier aufrufen, zB postProAnz(value) value = neue ProAnz
    return value
  }

  /*updateProduktAnzahl = function(anz,id){
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/setProAnzInv',
      data: {ProAnz:anz,productid:id}
    }).then(function successCallback(response){
      console.log(response);
      $scope.updateInventoryGet
    }, function errorCallback(response){
      console.log(response.data);
    });
  }*/

  /*$scope.updateProduktAnzahlNegativeGet = function(anz,id) {
    if (anz>0) {
    anz = anz-1
    }
    el = "ProAnz"+id;
    $scope.el = anz
    $scope.updateProduktAnzahlNegativePost(anz)
  }*/


/*  $scope.updateProduktAnzahlNegativePost = function(anz){
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/setProAnzInv',
      data: {ProAnz:anz}
    }).then(function successCallback(response){
      $scope.updateInventoryGet()
      console.log(response);
    }, function errorCallback(response){
      console.log(response.data);
    });
  }*/

  $scope.deleteProductFromInventory = function(product_id,inventory_id) {
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/deleteProductInv',
      data: {'pro_id':product_id,'inv_id':inventory_id}
    }).then(function successCallback(response){
      $window.location.reload();
      console.log(response);
    }, function errorCallback(response){
      console.log(response.data);
    });


  }
  /*$scope.adduser = function(){
    $http({
      method:'post',
      url: 'http://testhic-foi15a1.rhcloud.com/api/adduser',
      data: {'name': 'manfred','email': 'manfred@asfgfhdbrt.de'}
    }).then(function successCallback(response){
      console.log(response);
    }, function errorCallback(response){
      console.log(response);
    });
  }*/

})

.controller('kameraCtrl', function($scope) {

})

.controller('scanErgebnisCtrl', function($scope) {

})

.controller('profileCtrl', function($scope) {

})

.controller('keinCodeInDBVorhandenCtrl', function($scope) {

})
